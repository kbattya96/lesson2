import Resolver from '@forge/resolver';
import { storage } from '@forge/api';

const resolver = new Resolver();

resolver.define('getText', (req) => {
  console.log(req);

  return 'ok';
});


resolver.define('storeData', ({ payload }) => {
  console.log(payload);
  storage.set('usermessage', payload.formData.userMessage);

  return 'ok';
});

resolver.define('showData', async () => {
  const inputMessage = await storage.get('usermessage');

  return inputMessage;
});

export const handler = resolver.getDefinitions();
