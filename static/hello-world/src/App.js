import React, { useEffect, useState } from 'react';
import { invoke } from '@forge/bridge';
import FormDefaultExample from './Form';

function App() {
  const [data, setData] = useState(null);

  useEffect(() => {
    invoke('getText', { example: 'my-invoke-variable' }).then(setData);
  
  }, []);

  return (
    <div>
      {!data ? 'Loading...' : <FormDefaultExample/>}
    </div>
  );
}

export default App;
