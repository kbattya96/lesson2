import React, { Fragment } from 'react';
import { invoke } from '@forge/bridge';

import Button from '@atlaskit/button/standard-button';
import Textfield from '@atlaskit/textfield';
import Form, { Field, FormFooter } from '@atlaskit/form';


const FormDefaultExample = () => {

  return (
    <div>
      <Form
        onSubmit={(data) => {
          console.log('form submitted', data);
          invoke('storeData', { formData: data }).then(() => {
            invoke('showData').then((res) => {
              console.log(res);
            });
          });
          
        }}
      >
      {({ formProps }) => (
        <form {...formProps}>
          <Field label="Your message" name="userMessage">
            {({ fieldProps }) => (
              <Fragment>
                <Textfield
                  placeholder="Enter your details here"
                  {...fieldProps}
                />
              </Fragment>
            )}
          </Field>
          <FormFooter>
            <Button type="submit" appearance="primary">
              Submit
            </Button>
          </FormFooter>
        </form>
      )}
      </Form>
    </div>
  )
}


export default FormDefaultExample;

